//Resources
// https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Basic_animations
// https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Math/random

let canvasXSize = 256;
let canvasYSize = 256;
let renderMs = 1;
let scale = 0.33;
let y = 0;
// to press top analize this number for get min
let positionGame = -1;


//Main Program
// 
const canvas1 = document.getElementById('game');
canvas1.width = canvasXSize;
canvas1.height = canvasYSize;
// @ts-ignore
const context1 = canvas1.getContext('2d');

const imgTop = document.getElementById('img_top');
imgTop.width = canvasXSize * 3;
imgTop.height = canvasYSize;

let dx = 8;

class Img {

    constructor (image){
        this.image = image;
        this.imgW = 0;
        this.imgH = 0;
        this.x = 0;
        this.clearX = 0;
        this.clearY = 0;

        this.imgW = image.width * scale;
        this.imgH = image.height * scale;
        console.log(this.imgH + " " + this.imgW)
    
        if (this.imgW > canvasXSize) {
            // image larger than canvas
            this.x = canvasXSize - this.imgW;
        }
        if (this.imgW > canvasXSize) {
            // image width larger than canvas
            this.clearX = this.imgW;
        } else {
            this.clearX = canvasXSize;
        }
        if (this.imgH > canvasYSize) {
            // image height larger than canvas
            this.clearY = this.imgH;
        } else {
            this.clearY = canvasYSize;
        }
    } 
    getImage(){
        return this.image;
    }
    getClearX(){
        return this.clearX;
    }
    getClearY(){
        return this.clearY;
    }
    getImgW(){
        return this.imgW;
    }
    getImgH(){
        return this.imgH;
    }
    getX(){
        return this.x;
    }
}

//Buffer
let canvas2 = document.createElement('canvas');
canvas2.width = canvasXSize;
canvas2.height = canvasYSize;
let context2 = canvas2.getContext('2d');

let topIsRunning = true;

function randomRange(min, max){
    return Math.random() * (max - min) + min;
}

function drawBuffer(ctx){ 
    /*for (let index = 0; index < 3; index++){
        let oddNumber = index % 2; 
        if (oddNumber === 1 ){
            console.log("odd")
        }
    }*/
    animatedImage(0, imgTop, ctx, randomRange(4, dx), randomRange(0, imgTop.width), y);
    reverseAnimatedImage(1, imgTop, ctx, randomRange(4, dx), randomRange(0, imgTop.width / 2), imgTop.height * 0.33);
    animatedImage(2, imgTop, ctx, randomRange(4, dx), randomRange(0, imgTop.width), imgTop.height * 0.65);
}
/*
    @animatedImage(index, imgaeToRender, canvas context, spped, intial x position, initial y position )
    Draw a image in loop
*/
function animatedImage(index, image, ctx, speed, positionX, positionY){
    let imgObject = new Img(image);
    let imgW = imgObject.getImgW();
    let imgH = imgObject.getImgH();
    let x = imgObject.getX() + positionX;
    let img = imgObject.getImage();

    let loop = setInterval(()=>{
        context1.clearRect(0, 0, imgObject.getClearX(), imgObject.getClearY());
        // if image is <= Canvas Size
        if (imgW <= canvasXSize) {
            // reset, start from beginning
            if (x > canvasXSize) {
                x = -imgW + x;
            }
            // draw additional image1
            if (x > 0) {
                ctx.drawImage(img, -imgW + x, positionY, imgW, imgH);
            }
            // draw additional image2
            if (x - imgW > 0) {
                ctx.drawImage(img, -imgW * 2 + x, positionY, imgW, imgH);
            }
        } else { // image is > Canvas Size
            // reset, start from beginning
            if (x > (canvasXSize)) {
                x = canvasXSize - imgW;
            }
            // draw aditional image
            if (x > (canvasXSize - imgW)) {
                ctx.drawImage(img, x - imgW + 1, positionY, imgW, imgH);
            }
        }
        // draw image
        ctx.drawImage(img, x, positionY, imgW, imgH);
        // amount to move
        x += speed;

        if (index === positionGame){
            clearInterval(loop);
        }
    }, renderMs);
}

function reverseAnimatedImage(index, image, ctx, speed, positionX, positionY){
    let imgObject = new Img(image);
    let imgW = imgObject.getImgW();
    let imgH = imgObject.getImgH();
    let x = imgObject.getX() + positionX;
    let img = imgObject.getImage();

    let loop = setInterval(()=>{
        context1.clearRect(0, 0, imgObject.getClearX(), imgObject.getClearY());
        // if image is <= Canvas Size
        if (imgW <= canvasXSize) {
            // reset, start from beginning
            if (x < 0) {
                x = imgW * 2;
            }
            // draw additional image1
            if (x > 0) {
                ctx.drawImage(img, -imgW + x, positionY, imgW, imgH);
            }
            // draw additional image2
            if (x - imgW > 0) {
                ctx.drawImage(img, -imgW * 2 + x, positionY, imgW, imgH);
            }
        } else { // image is > Canvas Size
            // reset, start from beginning
            if (x > (canvasXSize)) {
                x = canvasXSize - imgW;
            }
            // draw aditional image
            if (x > (canvasXSize - imgW)) {
                ctx.drawImage(img, x - imgW + 1, positionY, imgW, imgH);
            }
        }
        // draw image
        ctx.drawImage(img, x, positionY, imgW, imgH);
        // amount to move
        x -= speed;
        //console.log(x)
        if (index === positionGame){
            clearInterval(loop);
        }
    }, renderMs);
}

function draw(){ 
    context1.drawImage(canvas2, 0, 0);
    requestAnimationFrame(draw);
}

function stop(){
    positionGame++;
}
drawBuffer(context2);
draw();